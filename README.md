# Continuous Integration Testing

## Description

Simple GitLab CI pipeline for test automation: [.gitlab-ci.yml ](https://gitlab.com/mrakbk/mrakbk.gitlab.io/-/blob/83a23ffcb5e79e67547cd9b20fc1c3ecc8e70416/.gitlab-ci.yml)

## Libraries
The tests are handled by the ```pytest``` library and plugins:
* ```pytest-html``` to generate an HTML report of the test results;
* ```pytest-xdist``` to distribute tests across multiple CPUs.


